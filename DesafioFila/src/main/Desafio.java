package main;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Desafio {

	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Queue<String> filaAlunos = new LinkedList<String>();
		String aluno;

		System.out.println("LISTA DE ALUNOS");
		char op = 's';
		while(op == 's') {
			System.out.println("Insira o nome do aluno: ");
			aluno = scanner.nextLine();
			filaAlunos.add(aluno);

			System.out.println("Deseja adicionar mais um aluno? \n(s) sim (n) n�o");
			op = scanner.nextLine().charAt(0);
		}

		if(filaAlunos.isEmpty()) {
			System.out.println("A fila existe e est� vazia");
		} else {
			System.out.println("O tamanho da fila �: " + filaAlunos.size());
		}
		
		System.out.println("O primeiro aluno da fila �: \n" + filaAlunos.peek());

		if(filaAlunos.size() > 0) {
			filaAlunos.remove();
		}
		
		System.out.println("Removendo...");
		System.out.println("O tamanho da fila �: " + filaAlunos.size());
		
		System.out.println("O primeiro aluno da fila agora �: \n" + filaAlunos.peek());

		System.out.println("Os alunos que est�o na fila s�o: ");
		Iterator<String> it = filaAlunos.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
}
